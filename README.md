![image](asyncapi.png)



# Tutorial y prueba de concepto usando AsyncAPI
Proof of concept to test the AsyncAPI Spect version 2.0

> La especificación AsyncAPI es un proyecto que se utiliza para describir y documentar las API basadas en mensajes en un formato legible por máquina. Es independiente del protocolo, por lo que puede usarlo para API que funcionan con cualquier protocolo. (AMQP, MQTT, WebSockets, Kafka, STOMP, HTTP, etc).

> La especificación AsyncAPI define un conjunto de archivos necesarios para describir dicha API. Estos archivos se pueden usar para crear utilidades, como herramientas de documentación, integración y/o prueba.


[Documentacion oficial...](https://www.asyncapi.com/docs/reference/specification/v2.0.0)
